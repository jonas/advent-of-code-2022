package app.jonas.adventofcode2022.d09

import java.io.File
import kotlin.math.*

private val <T> Pair<T, T>.x: T
    get() = first

private val <T> Pair<T, T>.y: T
    get() = second

fun part1(file: File): Long {
    val instructions = file.readLines().map { line ->
        val (direction, length) = line.split(" ")
        direction to length.toInt()
    }
    var head = 0 to 0
    var tail = 0 to 0
    val visited = mutableSetOf(tail)

    instructions.forEach { (direction, length) ->
        repeat(length) {
            head = when (direction) {
                "L" -> head.x - 1 to head.y
                "U" -> head.x to head.y - 1
                "R" -> head.x + 1 to head.y
                "D" -> head.x to head.y + 1
                else -> error("Unknown direction")
            }
            val distance = sqrt(
                (head.x.toDouble() - tail.x).pow(2) +
                        (head.y.toDouble() - tail.y).pow(2)
            )
            if (distance > 1.5) {
                val s = (head.x - tail.x).toDouble() / distance to (head.y - tail.y).toDouble() / distance
                tail = tail.x + round(s.x).toInt() to tail.y + round(s.y).toInt()
                println(s)
                if (distance > 2) {
                    when (direction) {
                        "R", "L" -> tail = tail.x to head.y
                        "U", "D" -> tail = head.x to tail.y
                    }
                }
                visited.add(tail)
                visited.size
            } else println("does not moves")
        }
    }

    return visited.size.toLong()
}

fun part2(file: File): Long {
    val instructions = file.readLines().map { line ->
        val (direction, length) = line.split(" ")
        direction to length.toInt()
    }

    val knots = MutableList(10) { 0 to 0 }
    val visited = mutableSetOf(knots[9])

    fun List<Pair<Int, Int>>.print() {
        for (y in -5..0) {
            for (x in 0..6) {
                print(if (contains(x to y)) "x" else ".")
            }
            println()
        }
        println()
    }

    fun follow(head: Pair<Int, Int>, tail: Pair<Int, Int>): Pair<Int, Int> {
        val distance = sqrt(
            (head.x.toDouble() - tail.x).pow(2) +
                    (head.y.toDouble() - tail.y).pow(2)
        )
        if (distance > 1.5) {
            val s = (head.x - tail.x).toDouble() / distance to (head.y - tail.y).toDouble() / distance
            val d = atan2(s.y, s.x)
            var newTail = tail.x + round(s.x).toInt() to tail.y + round(s.y).toInt()
            if (distance in 2.1..2.5) {
                val direction = when (d) {
                    in (PI / 4)..(3 * PI / 4) -> "D"
                    in -(PI / 4)..(PI / 4) -> "R"
                    in -(3 * PI / 4)..-(PI / 4) -> "U"
                    else -> "L"
                }
                newTail = when (direction) {
                    "R", "L" -> newTail.x to head.y
                    "U", "D" -> head.x to newTail.y
                    else -> error("Unknown direction")
                }
            }
            return newTail
        }
        return tail
    }

    instructions.forEach { (direction, length) ->
        repeat(length) {
            knots[0] = when (direction) {
                "L" -> knots[0].x - 1 to knots[0].y
                "U" -> knots[0].x to knots[0].y - 1
                "R" -> knots[0].x + 1 to knots[0].y
                "D" -> knots[0].x to knots[0].y + 1
                else -> error("Unknown direction")
            }
            //fun horizontal(head: Pair<Int, Int>, tail: Pair<Int, Int>)
            knots.drop(1).forEachIndexed { i, tail ->
                val head = knots[i + 1 - 1]
                knots[i + 1] = follow(head, tail)
                knots.print()
            }
            visited.add(knots[9])
        }
    }

    return visited.size.toLong()
}
