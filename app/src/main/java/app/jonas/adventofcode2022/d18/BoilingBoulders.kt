package app.jonas.adventofcode2022.d18

import java.io.File
import kotlin.math.max
import kotlin.math.min

private val <T> Triple<T, T, T>.x: T
    get() = first

private val <T> Triple<T, T, T>.y: T
    get() = second

private val <T> Triple<T, T, T>.z: T
    get() = third

typealias Cube = Triple<Int, Int, Int>

val Cube.adjacentCubes: List<Cube>
    get() = buildList {
        for (aX in (x - 1)..(x + 1) step 2)
            add(Cube(aX, y, z))
        for (aY in (y - 1)..(y + 1) step 2)
            add(Cube(x, aY, z))
        for (aZ in (z - 1)..(z + 1) step 2)
            add(Cube(x, y, aZ))
    }

private fun String.toCube(): Cube {
    val (x, y, z) = split(',').map(String::toInt)
    return Cube(x, y, z)
}

fun part1(file: File): Long {
    val cubes = file.readLines().map(String::toCube)
    return cubes.fold(0L) { acc, cube ->
        acc + 6 - cube.adjacentCubes.count(cubes::contains)
    }
}

fun part2(file: File): Long {
    val cubes = file.readLines().map(String::toCube)
    val (min, max) = cubes.fold(
        Triple(Int.MAX_VALUE, Int.MAX_VALUE, Int.MAX_VALUE)
                to Triple(Int.MIN_VALUE, Int.MIN_VALUE, Int.MIN_VALUE)
    ) { (min, max), cube ->
        Triple(min(min.x, cube.x), min(min.y, cube.y), min(min.z, cube.z)) to
                Triple(max(max.x, cube.x), max(max.y, cube.y), max(max.z, cube.z))
    }
    val xRange = min.x - 1..max.x + 1
    val yRange = min.y - 1..max.y + 1
    val zRange = min.z - 1..max.z + 1

    val queue = ArrayDeque<Cube>()
    val root = Cube(xRange.first, yRange.first, zRange.first)
    queue.add(root)
    val alreadyVisited = cubes.toHashSet()
    var surface = 0L
    while (queue.isNotEmpty()) {
        val current = queue.removeFirst()
        if (current in alreadyVisited) continue
        alreadyVisited.add(current)
        for (adjacent in current.adjacentCubes
            .filter { it.x in xRange && it.y in yRange && it.z in zRange }) {
            if (adjacent in cubes) surface++
            else queue.add(adjacent)
        }
    }
    return surface
}


