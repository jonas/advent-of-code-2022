package app.jonas.adventofcode2022.d05

import java.io.File

fun part1(file: File): String {
    val (stacksInput, instructionsInput) = file.readText().split("\n\n")
        .map { it.split('\n') }
    val stacks = stacksInput.dropLast(1).fold(mutableListOf(ArrayDeque<Char>(13))) {
            acc, element ->
        val crates = element.chunked(4).map { it[1] }

        crates.forEachIndexed { i, crate ->
            if (acc.lastIndex < i) acc.add(ArrayDeque(13))
            if (crate != ' ') acc[i].add(crate)
        }
        acc
    }
    val pattern = Regex("move (\\d+) from (\\d+) to (\\d+)")
    instructionsInput.forEach { instruction ->
        if (instruction.isEmpty()) return@forEach
        val (amount, start, end) = pattern.matchEntire(instruction)?.destructured
            ?: error("Invalid instruction")
        repeat(amount.toInt()) {
            stacks[end.toInt() - 1].addFirst(stacks[start.toInt() - 1].removeFirst())
        }
    }
    println(stacks)
    return stacks.map { it.first() }.joinToString("")
}

fun part2(file: File): String {
    val (stacksInput, instructionsInput) = file.readText().split("\n\n")
        .map { it.split('\n') }
    val stacks = stacksInput.dropLast(1).foldIndexed(mutableListOf(ArrayDeque<Char>(13))) {
            index, acc, element ->
        val cargos = element.chunked(4).map { it[1] }

        cargos.forEachIndexed { i, c ->
            if (acc.lastIndex < i) acc.add(ArrayDeque(13))
            if (c != ' ') acc[i].add(c)
        }
        acc
    }
    val pattern = Regex("move (\\d+) from (\\d+) to (\\d+)")
    instructionsInput.forEach { instruction ->
        if (instruction.isEmpty()) return@forEach
        val (amount, start, end) = pattern.matchEntire(instruction)?.destructured
            ?: error("Invalid instruction")
        val inter = mutableListOf<Char>()
        repeat(amount.toInt()) {

            inter.add(stacks[start.toInt() - 1].removeFirst())
        }
        inter.reversed().forEach {
            stacks[end.toInt() - 1].addFirst(it)
        }
    }
    println(stacks)
    return stacks.map { it.first() }.joinToString("")
}
