package app.jonas.adventofcode2022.d04

import java.io.File

fun part1(file: File) = file.readLines().count { line ->
    val (s1, e1, s2, e2) = line.split('-', ',').map(String::toInt)
    val r1 = s1..e1
    val r2 = s2..e2
    r1.all { r2.contains(it) } || r2.all { r1.contains(it) }
}

fun part2(file: File) = file.readLines().count { line ->
    val (s1, e1, s2, e2) = line.split('-', ',').map(String::toInt)
    val r1 = s1..e1
    val r2 = s2..e2
    r1.any { r2.contains(it) } || r2.any { r1.contains(it) }
}
