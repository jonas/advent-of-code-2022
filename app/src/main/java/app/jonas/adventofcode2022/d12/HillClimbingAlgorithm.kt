package app.jonas.adventofcode2022.d12

import java.io.File
import java.lang.Long.min

private val <T> Pair<T, T>.x: T
    get() = first

private val <T> Pair<T, T>.y: T
    get() = second

lateinit var start: Pair<Int, Int>
lateinit var end: Pair<Int, Int>

fun part1(file: File): Long {
    val heightmap = file.readLines().mapIndexed { y, row ->
        row.toCharArray().also {
            val sX = row.indexOf('S')
            if (sX != -1) {
                start = sX to y
                it[sX] = 'a'
            }
            val eX = row.indexOf('E')
            if (eX != -1) {
                end = eX to y
                it[eX] = 'z'
            }
        }
    }
    val fromThere = hashMapOf(start to Long.MAX_VALUE / 2)
    val visited = hashSetOf(start)

    val debug = Array(heightmap.size) {
        IntArray(heightmap.first().size)
    }

    fun climb(/*from: List<Pair<Int, Int>>,*/ position: Pair<Int, Int>, stepsUntil: Long): Long {
        fromThere[position] = Long.MAX_VALUE
        val (x, y) = position
        // if (from.size % 100 == 0) println(from.size)
        if (position == end) {
            /*from.mapIndexed { index, (dx, dy) ->
                debug[dy][dx] = index
            }*/
            return stepsUntil + 1
        }
        val height = heightmap[y][x]
        return listOf((x - 1) to y, x to (y - 1), x + 1 to y, x to (y + 1)).minOf { next ->
            if (next.x < 0 || next.x > heightmap[0].lastIndex ||
                next.y < 0 || next.y > heightmap.lastIndex ||
                heightmap[next.y][next.x] - height > 1
                // (visited.contains(next) && fromThere[next] != null)
            ) return@minOf Long.MAX_VALUE
            val already = fromThere[next]
            if (already == Long.MAX_VALUE) return@minOf Long.MAX_VALUE
            if (already != null) return@minOf stepsUntil + already
            climb(/*from + position,*/ next, stepsUntil + 1).also {
                fromThere[position] = min(fromThere[position]!!, it - stepsUntil)
            }
        }
    }

    return climb( start, 0).also {
        debug
    }
}

fun part2(file: File): Long {

    return 1L
}
