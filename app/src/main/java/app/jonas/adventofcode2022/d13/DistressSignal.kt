package app.jonas.adventofcode2022.d13

import java.io.File

sealed class Leave
data class SingleLeave(val value: Int): Leave()

private class Tree(
    val leaves: MutableList<Leave> = mutableListOf(),
    val parent: Tree? = null
) : Leave(), Comparable<Tree> {

    override fun compareTo(other: Tree): Int {
        leaves.forEachIndexed {
                i, leave ->
            val otherLeave = other.leaves.getOrNull(i) ?: return 1
            if (leave is SingleLeave && otherLeave is SingleLeave) {
                if (leave.value - otherLeave.value < 0) return -1
                if (leave.value - otherLeave.value > 0) return 1
            }
            if (leave is Tree && otherLeave is Tree) {
                if (leave < otherLeave) return -1
                if (leave > otherLeave) return 1
            }
            if (leave is SingleLeave && otherLeave is Tree) {
                if (Tree(mutableListOf(leave)) < otherLeave) return -1
                if (Tree(mutableListOf(leave)) > otherLeave) return 1
            }
            if (leave is Tree && otherLeave is SingleLeave) {
                if (leave < Tree(mutableListOf(otherLeave))) return -1
                if (leave > Tree(mutableListOf(otherLeave))) return 1
            }
        }
        return leaves.size - other.leaves.size
    }

    companion object {
        fun fromInstructions(instructions: String): Tree {
            val tree = Tree()
            var currentTree = tree
            var currentIntInstruction: String? = null
            instructions.drop(1).forEach { c: Char ->
                if (c.isDigit()) {
                    currentIntInstruction = (currentIntInstruction ?: "") + c
                    return@forEach
                }
                currentIntInstruction?.let { intInstruction ->
                    currentTree.leaves.add(SingleLeave(intInstruction.toInt()))
                    currentIntInstruction = null
                }
                when (c) {
                    '[' -> {
                        val newTree = Tree(parent = currentTree)
                        currentTree.leaves.add(newTree)
                        currentTree = newTree
                    }
                    ']' -> currentTree = currentTree.parent ?: currentTree
                }
            }
            return tree
        }
    }
}

fun part1(file: File): Long {
    return file.readLines().chunked(3).mapIndexed { i, (packageInstructions1, packageInstructions2) ->
        val package1 = Tree.fromInstructions(packageInstructions1)
        val package2 = Tree.fromInstructions(packageInstructions2)

        if (package1 < package2) i + 1L else 0
    }.sum()
}

fun part2(file: File): Long {
    val dividers = listOf("[[2]]", "[[6]]")
    val packets = file.readText()
        .replace("\n\n", "\n")
        .split('\n') + dividers
    val sorted = packets.sortedBy { Tree.fromInstructions(it) }
    val x = sorted.indexOf(dividers.first()) + 1L
    val y = sorted.indexOf(dividers[1]) + 1L
    return x * y
}
