package app.jonas.adventofcode2022.d01

import java.io.File

fun part1(file: File): Int {
    return file.readText()
        .split("\n\n")
        .maxOf { elfCalories -> elfCalories.split("\n").map(String::toInt).sum() }
}

fun part2(file: File): Int {
    return file.readText()
        .split("\n\n")
        .map { elfCalories -> elfCalories.split("\n").map(String::toInt).sum() }
        .sortedDescending().take(3).sum()
}