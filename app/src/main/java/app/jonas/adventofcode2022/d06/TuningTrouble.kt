package app.jonas.adventofcode2022.d06

import java.io.File

fun part1(file: File): Long {
    file.readLines()[0].windowed(4).withIndex().forEach {
            (index: Int, window: String) ->
        if (window.toSet().size == 4) return index + 4L
    }
    return 1L
}

fun part2(file: File): Long {
    file.readLines()[0].windowed(14).withIndex().forEach {
            (index: Int, window: String) ->
        if (window.toSet().size == 14) return index + 14L
    }
    return 1L
}
