package app.jonas.adventofcode2022.d14

import java.io.File
import java.util.Collections.max
import java.util.Collections.min

private val <T> Pair<T, T>.x: T
    get() = first

private val <T> Pair<T, T>.y: T
    get() = second

fun part1(file: File): Long {

    val grid = hashMapOf<Int, MutableList<Int>>()
    var minX = Int.MAX_VALUE
    var maxX = 0
    var minY = 0
    var maxY = 0

    file.readLines().map { line ->
        line.split(" -> ").reduce { lastPoint, point ->
            val (xL, yL) = lastPoint.split(',').map(String::toInt)
            val (x, y) = point.split(',').map(String::toInt)
            minX = min(listOf(minX, xL, x))
            maxX = max(listOf(maxX, xL, x))
            minY = min(listOf(minY, yL, y))
            maxY = max(listOf(maxY, yL, y))
            if (xL == x) {
                (min(listOf(yL, y))..max(listOf(yL, y))).forEach {
                    if (grid[it] == null) {
                        grid[it] = mutableListOf(x)
                    } else {
                        grid[it]?.add(x)
                    }
                }
            } else {
                (min(listOf(xL, x))..max(listOf(xL, x))).forEach {
                    if (grid[y] == null) {
                        grid[y] = mutableListOf(it)
                    } else {
                        grid[y]?.add(it)
                    }
                }
            }
            point
        }
    }

    fun print() {
        (minY..maxY).forEach { y ->
            (minX..maxX).forEach { x ->
                val c = if (grid[y]?.contains(x) == true) '#' else '.'
                print(c)
            }
            println()
        }
    }

    var currentSand = 500 to 0
    var count = 0L

    print()

    while (currentSand.x in minX..maxX && currentSand.y < maxY) {
        if (grid[currentSand.y + 1]?.contains(currentSand.x) != true) {
            currentSand = currentSand.x to currentSand.y + 1
        } else if (grid[currentSand.y + 1]?.contains(currentSand.x - 1) != true) {
            currentSand = currentSand.x - 1 to currentSand.y + 1
        } else if (grid[currentSand.y + 1]?.contains(currentSand.x + 1) != true) {
            currentSand = currentSand.x + 1 to currentSand.y + 1
        } else {
            if (grid[currentSand.y] == null) {
                grid[currentSand.y] = mutableListOf(currentSand.x)
            } else {
                grid[currentSand.y]?.add(currentSand.x)
            }
            count++
            currentSand = 500 to 0
        }
    }

    print()

    return count
}

fun part2(file: File): Long {
    val grid = hashMapOf<Int, MutableList<Int>>()
    var minX = Int.MAX_VALUE
    var maxX = 0
    var minY = 0
    var maxY = 0

    file.readLines().map { line ->
        line.split(" -> ").reduce { lastPoint, point ->
            val (xL, yL) = lastPoint.split(',').map(String::toInt)
            val (x, y) = point.split(',').map(String::toInt)
            minX = min(listOf(minX, xL, x))
            maxX = max(listOf(maxX, xL, x))
            minY = min(listOf(minY, yL, y))
            maxY = max(listOf(maxY, yL, y))
            if (xL == x) {
                (min(listOf(yL, y))..max(listOf(yL, y))).forEach {
                    if (grid[it] == null) {
                        grid[it] = mutableListOf(x)
                    } else {
                        grid[it]?.add(x)
                    }
                }
            } else {
                (min(listOf(xL, x))..max(listOf(xL, x))).forEach {
                    if (grid[y] == null) {
                        grid[y] = mutableListOf(it)
                    } else {
                        grid[y]?.add(it)
                    }
                }
            }
            point
        }
    }

    fun print() {
        (minY..maxY).forEach { y ->
            (minX..maxX).forEach { x ->
                val c = if (grid[y]?.contains(x) == true) '#' else '.'
                print(c)
            }
            println()
        }
    }

    var currentSand = 500 to 0
    var count = 0L

    print()

    val bottom = maxY + 2

    while (true) {
        val onBottom = currentSand.y + 1 == bottom
        if (!onBottom && grid[currentSand.y + 1]?.contains(currentSand.x) != true) {
            currentSand = currentSand.x to currentSand.y + 1
        } else if (!onBottom && grid[currentSand.y + 1]?.contains(currentSand.x - 1) != true) {
            currentSand = currentSand.x - 1 to currentSand.y + 1
        } else if (!onBottom && grid[currentSand.y + 1]?.contains(currentSand.x + 1) != true) {
            currentSand = currentSand.x + 1 to currentSand.y + 1
        } else {
            count++
            if (currentSand == 500 to 0) return count
            if (grid[currentSand.y] == null) {
                grid[currentSand.y] = mutableListOf(currentSand.x)
            } else {
                grid[currentSand.y]?.add(currentSand.x)
            }
            currentSand = 500 to 0
        }
    }
}
