package app.jonas.adventofcode2022.d03

import java.io.File

/*
Lowercase item types a through z have priorities 1 through 26.
Uppercase item types A through Z have priorities 27 through 52.
*/
fun Char.toPriority() = this - (if (isUpperCase()) 'A' - 27 else 'a' - 1)

fun part1(file: File) = file.readLines().sumOf { rucksack ->
    if (rucksack.isEmpty()) return@sumOf 0
    val compartments = rucksack.chunked(rucksack.length / 2).map(String::toSet)
    val letter = (compartments.first() intersect compartments[1]).first()
    letter.toPriority()
}

fun part2(file: File) = file.readLines().map(String::toSet).chunked(3).sumOf { rucksacks ->
    if (rucksacks.isEmpty()) return@sumOf 0
    val letter = (rucksacks.first() intersect rucksacks[1] intersect rucksacks[2]).first()
    letter.toPriority()
}