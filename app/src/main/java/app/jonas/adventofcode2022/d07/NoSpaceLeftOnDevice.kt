package app.jonas.adventofcode2022.d07

import java.io.File

class CFile(
    val name: String,
    val isDirectory: Boolean,
    val containing: MutableSet<CFile> = mutableSetOf(),
    val ownSize: Long = 0L,
    var parent: CFile? = null
) {
    val size: Long
     get() = if (isDirectory) containing.sumOf { it.size } else ownSize

    fun addFile(cFile: CFile) = containing.add(cFile).also { cFile.parent = this }

    override fun toString(): String {
        return "$name ($size) -> $containing"
    }
}

fun parseTree(file: File): CFile {
    val tree = CFile("/", isDirectory = true)
    var currentDir = tree
    file.readLines().forEach { line ->
        when {
            line.startsWith("$ cd /") -> {
                currentDir = tree
            }
            line.startsWith("$ cd ..") -> {
                currentDir = currentDir.parent ?: error("Already in top most directory")
            }
            line.startsWith("$ cd") -> {
                val (_, _, newDir) = line.split(' ')
                currentDir = currentDir.containing.first { it.name == newDir }
            }
            line.startsWith("$ ls") -> {}
            line.startsWith("dir") -> {
                val (_, dir) = line.split(' ')
                currentDir.addFile(CFile(name = dir, isDirectory = true))
            }
            else -> {
                if (line.isEmpty()) return@forEach
                val (size, fileName) = line.split(' ')
                currentDir.addFile(CFile(
                    name = fileName,
                    isDirectory = false,
                    ownSize = size.toLong()
                ))
            }
        }
    }
    return tree
}

fun part1(file: File): Long {
    val sizeAtMost = 100_000L
    val tree = parseTree(file)
    fun walkTree(cFile: CFile): Long {
        return (if (cFile.isDirectory && cFile.size < sizeAtMost) cFile.size else 0L) +
                cFile.containing.sumOf { walkTree(it) }
    }
    return walkTree(tree)
}

fun part2(file: File): Long {
    val tree = parseTree(file)

    val totalSpace = 70_000_000L
    val neededSpace = 30_000_000L
    val availableSpace = totalSpace - tree.size
    val spaceLeftNeeded = neededSpace - availableSpace

    val dirs = mutableListOf(tree)
    fun walkTree(cFile: CFile) {
        if (cFile.isDirectory && cFile.size > spaceLeftNeeded) {
            dirs.add(cFile)
        }
        cFile.containing.forEach { walkTree(it) }
    }
    walkTree(tree)
    return dirs.minBy { it.size }.size
}
