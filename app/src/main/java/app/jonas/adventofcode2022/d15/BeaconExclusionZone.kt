package app.jonas.adventofcode2022.d15

import java.io.File
import kotlin.math.abs

private val <T> Pair<T, T>.x: T
    get() = first

private val <T> Pair<T, T>.y: T
    get() = second

fun part1(file: File, lineToCheck: Long): Long {
    val (ranges, existingBeaconsOnLine) = file.readLines()
        .fold(listOf<LongRange>() to listOf<Long>()) { acc, line ->
            val splitted = line.split("x=", ", y=", ":")
            val sensor = splitted[1].toLong() to splitted[2].toLong()
            val beacon = splitted[4].toLong() to splitted[5].toLong()
            val distance = abs(sensor.x - beacon.x) + abs(sensor.y - beacon.y)
            val distanceToLine = abs(sensor.y - lineToCheck)
            if (distanceToLine > distance) return@fold acc
            val intersectionLength = distance - distanceToLine
            acc.first + listOf((sensor.x - intersectionLength)..(sensor.x + intersectionLength)) to
                    if (beacon.y == lineToCheck) acc.second + beacon.x else acc.second
        }

    val min = ranges.minOf { it.first }
    val max = ranges.maxOf { it.last }
    return (min..max).count { x ->
        !existingBeaconsOnLine.contains(x) &&
                ranges.any { it.contains(x) }
    }.toLong()
}

fun part2(file: File, maxDistance: Long): Long {
    fun checkLine(lineToCheck: Long) = file.readLines()
        .fold(listOf<LongRange>() to listOf<Long>()) { acc, line ->
            val splitted = line.split("x=", ", y=", ":")
            val sensor = splitted[1].toLong() to splitted[2].toLong()
            val beacon = splitted[4].toLong() to splitted[5].toLong()
            val distance = abs(sensor.x - beacon.x) + abs(sensor.y - beacon.y)
            val distanceToLine = abs(sensor.y - lineToCheck)
            if (distanceToLine > distance) return@fold acc
            val intersectionLength = distance - distanceToLine
            acc.first + listOf((sensor.x - intersectionLength)..(sensor.x + intersectionLength)) to
                    if (beacon.y == lineToCheck) acc.second + beacon.x else acc.second
        }

    (0..maxDistance).forEach { y ->
        if (y % 100L == 0L) println(y)
        val (ranges, existingBeaconsOnLine) = checkLine(lineToCheck = y)
        (0..maxDistance).forEach { x ->
            if (!existingBeaconsOnLine.contains(x) && ranges.none { it.contains(x) }) {
                return x * 4_000_000L + y
            }
        }
    }
    error("Tuning signal not found in area.")
}
