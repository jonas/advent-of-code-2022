package app.jonas.adventofcode2022.d02

import java.io.File

/*
Rock        A   X   1
Paper       B   Y   2
Scissors    C   Z   3

Win         6
Draw        3
Loss        0
*/

fun part1(file: File): Int {
    val points: List<Int> = file.readLines().map {
        val (opponent, self) = it.split(" ")
        val opponentShifted = opponent.first() - ('A' - '1')
        val selfShifted = self.first() - ('X' - '1')
        println("opponent: $opponent")
        println("self: $self")
        println("opponentShifted: $opponentShifted")
        println("selfShifted: $selfShifted")
        val win = when (selfShifted) {
            '1' -> opponentShifted == '3'
            '2' -> opponentShifted == '1'
            '3' -> opponentShifted == '2'
            else -> throw IllegalArgumentException()
        }
        val draw = selfShifted == opponentShifted
        println("win: $win")
        println("draw: $draw")
        val pointsWinning = if (win) 6 else if (draw) 3 else 0
        println("pointsWinning: $pointsWinning")
        println("selfShifted.code: ${selfShifted.code}")
        val points = pointsWinning + selfShifted.code - 48
        println("points: $points")
        points
    }

    return points.sum()
}

/*
X   Loss
Y   Draw
Z   Win
 */

fun part2(file: File): Int {
    val points: List<Int> = file.readLines().map {
        val (opponent, self) = it.split(" ")
        val opponentShifted = opponent.first() - ('A' - '1')
        println("opponent: $opponent")
        println("self: $self")
        println("opponentShifted: $opponentShifted")
        val selfShifted = when (self.first()) {
            'Z' -> when (opponentShifted) {
                '1' -> '2'
                '2' -> '3'
                '3' -> '1'
                else -> throw IllegalArgumentException()
            }
            'Y' -> opponentShifted
            'X' -> when (opponentShifted) {
                '1' -> '3'
                '2' -> '1'
                '3' -> '2'
                else -> throw IllegalArgumentException()
            }
            else -> throw IllegalArgumentException()
        }
        println("selfShifted: $selfShifted")
        val draw = self.first() == 'Y'
        val win = self.first() == 'Z'
        println("win: $win")
        println("draw: $draw")
        val pointsWinning = if (win) 6 else if (draw) 3 else 0
        println("pointsWinning: $pointsWinning")
        println("selfShifted.code: ${selfShifted.code}")
        val points = pointsWinning + selfShifted.code - 48
        println("points: $points")
        println()
        points
    }

    return points.sum()
}