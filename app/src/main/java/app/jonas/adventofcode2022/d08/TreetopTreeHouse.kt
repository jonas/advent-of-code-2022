package app.jonas.adventofcode2022.d08

import java.io.File

data class Tree(val size: Int, val position: Pair<Int, Int>)

fun part1(file: File): Long {
    val forrest = file.readLines().mapIndexed { y, line -> line.mapIndexed {
            x, tree -> Tree(size = tree.code - 48, position = x to y) }.toMutableList()
    }.toMutableList()
    val visibleTrees = mutableSetOf<Tree>()
    fun visibleTreesFromLeft() = forrest.forEach { line ->
        line.forEachIndexed { x, tree ->
            if (
                line.subList(fromIndex = 0, toIndex = x)
                    .all { otherTree -> tree.size > otherTree.size }
            ) visibleTrees.add(tree)
        }
    }

    fun List<List<Tree>>.print() {
        forEach { line ->
            line.forEach { print("${it.size}${if (visibleTrees.contains(it)) "^" else "o"}") }
            println()
        }
    }

    visibleTreesFromLeft()
    repeat(3) {
        forrest.rotate90Clockwise()
        visibleTreesFromLeft()
    }
    forrest.rotate90Clockwise()
    forrest.print()
    return visibleTrees.size.toLong()
}

fun <T> MutableList<MutableList<T>>.rotate90Clockwise() {
    val size = this[0].size
    // Traverse each cycle
    for (i in 0 until size / 2) {
        for (j in i until size - i - 1) {

            // Swap elements of each cycle
            // in clockwise direction
            val temp = this[i][j]
            this[i][j] = this[size - 1 - j][i]
            this[size - 1 - j][i] = this[size - 1 - i][size - 1 - j]
            this[size - 1 - i][size - 1 - j] = this[j][size - 1 - i]
            this[j][size - 1 - i] = temp
        }
    }
}

fun part2(file: File): Long {
    val forrest = file.readLines().mapIndexed { y, line -> line.mapIndexed {
            x, tree -> Tree(size = tree.code - 48, position = x to y) }.toMutableList()
    }.toMutableList()

    val scores = mutableMapOf<Tree, Long>()

    fun visibleTreesFromLeft() = forrest.forEach forrest@{ line ->
        line.forEachIndexed treeLine@{ x, tree ->
            var score = 0
            var i = x + 1
            var otherTree = line.getOrNull(i)
            while (otherTree != null && otherTree.size < tree.size) {
                score++
                i++
                otherTree = line.getOrNull(i)
            }
            if (otherTree != null) score += 1
            scores[tree] = scores.getOrElse(tree) {1} * score
        }
    }

    visibleTreesFromLeft()
    repeat(3) {
        forrest.rotate90Clockwise()
        visibleTreesFromLeft()
    }
    return scores.maxOf { (_, value) -> value }
}
