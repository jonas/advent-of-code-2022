package app.jonas.adventofcode2022.d11

import java.io.File
import java.math.BigInteger
import kotlin.math.floor

data class Monkey(
    val items: MutableList<BigInteger>,
    val operation: (BigInteger) -> BigInteger,
    val test: (BigInteger) -> Boolean,
    val targetTrue: Int,
    val targetFalse: Int,
    val divBy: BigInteger,
    var inspectedItems: BigInteger = BigInteger.ZERO
)

fun operationAdd(y: BigInteger) = { x: BigInteger -> x + y }
fun operationMultiply(y: BigInteger) = { x: BigInteger -> x * y }
val operationMultiplyByItself = { x: BigInteger -> x * x }

fun operationDivisibleBy(y: BigInteger) = { x: BigInteger -> x.remainder(y) == BigInteger.ZERO }

fun part1(file: File) = part2(file, rounds = 20, sanitizer = { newWorryLevel, _ ->
    BigInteger.valueOf(floor(newWorryLevel.toDouble() / 3).toLong())
})

fun part2(
    file: File,
    rounds: Int = 10_000,
    sanitizer: (BigInteger, BigInteger) -> BigInteger = { newWorryLevel, commonMultiple ->
        newWorryLevel % commonMultiple
    }
): BigInteger {
    val monkeys = file.readText().split("\n\n").map { monkeyInstructions ->
        val (
            itemsInstructions,
            operationInstruction,
            testInstruction,
            targetTrueInstruction,
            targetFalseInstruction,
        ) = monkeyInstructions.split('\n').drop(1)
        val items = itemsInstructions
            .substringAfter(": ")
            .split(", ")
            .map(String::toBigInteger)
            .toMutableList()
        val operation = when {
            operationInstruction.contains('+') ->
                operationAdd(operationInstruction.substringAfter("+ ").toBigInteger())
            operationInstruction.contains("* old") -> operationMultiplyByItself
            operationInstruction.contains('*') ->
                operationMultiply(operationInstruction.substringAfter("* ").toBigInteger())
            else -> error("Unknown operation!")
        }
        val divBy = testInstruction.substringAfter("by ").toBigInteger()
        val test = operationDivisibleBy(divBy)
        val targetTrue = targetTrueInstruction.substringAfter("monkey ").toInt()
        val targetFalse = targetFalseInstruction.substringAfter("monkey ").toInt()
        Monkey(items, operation, test, targetTrue, targetFalse, divBy)
    }
    val commonMultiple = monkeys.fold(BigInteger.ONE) { acc, monkey -> acc * monkey.divBy }
    repeat(rounds) {
        monkeys.forEach { monkey ->
            for (i in monkey.items.indices) {
                val worryLevel = monkey.items.removeFirst()
                val newWorryLevel = monkey.operation(worryLevel)
                val newWorryLevelSanitized = sanitizer(newWorryLevel, commonMultiple)
                val target = if (monkey.test(newWorryLevelSanitized)) {
                    monkey.targetTrue
                } else {
                    monkey.targetFalse
                }
                monkeys[target].items.add(newWorryLevelSanitized)
                monkey.inspectedItems++
            }
        }
        if (it % 100 == 0) println(it)
    }
    return monkeys.sortedByDescending { monkey -> monkey.inspectedItems }.take(2)
        .fold(BigInteger.ONE) { acc, monkey -> acc * monkey.inspectedItems }
}
