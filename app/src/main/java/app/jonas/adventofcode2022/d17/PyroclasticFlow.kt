package app.jonas.adventofcode2022.d17

import java.io.File
import kotlin.math.max

private val <T> Pair<T, T>.x: T
    get() = first

private val <T> Pair<T, T>.y: T
    get() = second

typealias Point = Pair<Long, Long>

/*
####

.#.
###
.#.

..#
..#
###

#
#
#
#

##
##
 */
private sealed class Rock(origin: Point) {
    open infix fun intersects(other: Rock): Boolean {
        return other.occupied.any { otherPoint ->
            occupied.any { it == otherPoint }
        }
    }

    var previousOrigin = origin
    var origin = origin
        set(value) {
            previousOrigin = origin
            field = value
        }
    fun reset() {
        origin = previousOrigin
    }

    abstract val occupied: Sequence<Point>
}

private class MinusRock(origin: Point): Rock(origin) {
    override val occupied: Sequence<Point>
        get() = sequence {
            yield(origin)
            repeat(3) {
                yield(origin.x + 1 + it to origin.y)
            }
        }
}

private class PlusRock(origin: Point): Rock(origin) {
    override val occupied: Sequence<Point>
        get() = sequence {
            yield(origin.x + 1 to origin.y)
            repeat(3) {
                yield(origin.x + it to origin.y + 1)
            }
            yield(origin.x + 1 to origin.y + 2)
        }
}

private class ArrowRock(origin: Point): Rock(origin) {
    override val occupied: Sequence<Point>
        get() = sequence {
            yield(origin)
            yield(origin.x + 1 to origin.y)
            yield(origin.x + 2 to origin.y)
            yield(origin.x + 2 to origin.y + 1)
            yield(origin.x + 2 to origin.y + 2)
        }
}

private class PipeRock(origin: Point): Rock(origin) {
    override val occupied: Sequence<Point>
        get() = sequence {
            yield(origin)
            repeat(3) {
                yield(origin.x to origin.y + 1 + it)
            }
        }
}

private class SquareRock(origin: Point): Rock(origin) {
    override val occupied: Sequence<Point>
        get() = sequence {
            yield(origin)
            yield(origin.x to origin.y + 1)
            yield(origin.x + 1 to origin.y)
            yield(origin.x + 1 to origin.y + 1)
        }
}

private object Chamber : Rock(0L to 0L) {
    override infix fun intersects(other: Rock): Boolean {
        return other.occupied.any { (x, y) -> x == 0L || x == 8L || y == 0L }
    }

    override val occupied: Sequence<Point>
        get() = error("occupies infinite points")

}

fun part1(file: File, amountOfStoppedRocks: Long = 2022): Long {
    var rocksToAppearIndex = 0
    val rocksToAppear = listOf(
        ::MinusRock,
        ::PlusRock,
        ::ArrowRock,
        ::PipeRock,
        ::SquareRock,
    )
    var jetPatternIndex = 0
    val jetPattern = file.readLines().first()

    val stoppedRocks = hashMapOf<Long, MutableList<Rock>>()
    var alreadyStopped = 1L
    var highestRock = 0L
    while (alreadyStopped <= amountOfStoppedRocks) {
        if (alreadyStopped % 1_000_000 == 0L)
            println(alreadyStopped)
        val rock = rocksToAppear[rocksToAppearIndex](3L to highestRock + 4)
        rocksToAppearIndex = (rocksToAppearIndex + 1) % rocksToAppear.size
        fun checkIntersections(): Boolean {
            if (Chamber intersects rock) return true
            for (y in (rock.origin.y - 4)..(rock.origin.y + 4)) {
                if (stoppedRocks[y]?.any { it intersects rock } == true) return true
            }
            return false
        }
        while (!checkIntersections()) {
            val direction = if (jetPattern[jetPatternIndex] == '>') 1 else -1
            jetPatternIndex = (jetPatternIndex + 1) % jetPattern.length
            rock.origin = rock.origin.x + direction to rock.origin.y
            if (checkIntersections()) rock.reset()
            rock.origin = rock.origin.x to rock.origin.y - 1
        }
        rock.reset()
        stoppedRocks.getOrPut(rock.origin.y) {
            mutableListOf()
        } += rock
        alreadyStopped++
        highestRock = max(highestRock, rock.occupied.maxOf { it.y })
    }

    return highestRock
}

fun part2(file: File) = part1(file, 1_000_000_000_000)
