package app.jonas.adventofcode2022.d10

import java.io.File

fun part1(file: File): Long {
    var i = 0L
    var x = 1L
    var result = 0L

    fun checkSignal() {
        if (i == 20L || i % 40L == 20L) {
            result += i * x
        }
    }

    file.readLines().forEach {
        val line = it.split(" ")
        when (line.first()) {
            "noop" -> {
                i++
                checkSignal()
            }
            "addx" -> {
                i++
                checkSignal()
                i++
                checkSignal()
                val amount = line[1].toInt()
                x += amount
            }
        }

    }
    return result
}

fun part2(file: File): String {
    var i = 0L
    var x = 1L
    var result = ""

    fun checkSignal() {
        if (i % 40L == 0L && i != 0L) result += '\n'
        result += if (i % 40L in (x-1)..(x+1)) '#' else '.'
    }

    file.readLines().forEach {
        val line = it.split(" ")
        when (line.first()) {
            "noop" -> {
                checkSignal()
                i++
            }
            "addx" -> {
                checkSignal()
                i++
                checkSignal()
                i++
                val amount = line[1].toInt()
                x += amount
            }
        }
    }
    return result.also { print(it) }
}
