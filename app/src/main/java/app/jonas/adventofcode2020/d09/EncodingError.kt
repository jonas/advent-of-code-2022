package app.jonas.adventofcode2020.d09

import java.io.File
import java.lang.Integer.min

fun part1(file: File, preamble: Int): Long {
    val numbers = file.readLines().map(String::toLong)
    var index = preamble
    val foundSummands = Array(numbers.size) { it < preamble }

    for (i in numbers.indices) {
        if (!foundSummands[i]) return numbers[i]
        val end = min(i + preamble, numbers.size)
        for (j in i + 1 until end) {
            val sum = numbers[i] + numbers[j]
            val kEnd = min(end + 1, numbers.size)
            for (k in (j + 1) until kEnd) {
                if (foundSummands[k]) continue
                if (numbers[k] == sum) foundSummands[k] = true
            }
        }
    }
    //used for checking for duplicates (there are some in the input)
    //println(numbers.count { numbers.indexOf(it) != numbers.lastIndexOf(it) })
    throw IllegalArgumentException()
}

fun part2(file: File, preamble: Int): Long {
    val nonSimpleSum = part1(file, preamble)
    val numbers = file.readLines().map(String::toLong)

    var startIndex = 0
    var endIndex = 1
    var sum = numbers[startIndex] + numbers[endIndex]
    while (sum != nonSimpleSum) {
        if (sum < nonSimpleSum) {
            endIndex++
            sum += numbers[endIndex]
        } else {
            sum -= numbers[startIndex]
            startIndex++
        }
    }
    val contiguousSummands = numbers.subList(startIndex, endIndex)
    return contiguousSummands.maxOrNull()!! + contiguousSummands.minOrNull()!!
}
