package app.jonas.adventofcode2020.d07

import java.io.File

// light red bags contain 1 bright white bag, 2 muted yellow bags.
val ruleRegex = Regex("(.+) bags? contain(.+)")
val containRegex = Regex(" (\\d+) (.+) bags?\\.?")

typealias Contain = List<Pair<Int, String>>

fun parseRule(rule: String): Pair<String, Contain> {
    val ruleMatchResult = ruleRegex.matchEntire(rule)?.groupValues
        ?: throw IllegalArgumentException()
    val color = ruleMatchResult[1]
    val contain = ruleMatchResult[2].split(',')
    val result = contain.mapNotNull { c ->
        val containedBagMatchResult = containRegex.matchEntire(c)?.groupValues
            ?: return@mapNotNull null // no other bags.
        val containedBagAmount = containedBagMatchResult[1].toInt()
        val containedBagColor = containedBagMatchResult[2]
        containedBagAmount to containedBagColor
    }
    return color to result
}

fun Map<String, Contain>.canHoldBag(holdingBag: String, containingBag: String): Boolean {
    val contain = get(holdingBag)
    return contain?.count { (_, color) ->
        color == containingBag || canHoldBag(
            color,
            containingBag
        )
    } != 0
}

fun part1(file: File) = file.readLines().map(::parseRule).toMap().let {
    var count = 0
    it.forEach { (color, _) ->
        if (it.canHoldBag(color, "shiny gold")) count++
    }
    count
}

fun Map<String, Contain>.countBags(holdingBag: String): Int {
    val contain = get(holdingBag) ?: throw IllegalArgumentException()
    return 1 + contain.map { (amount, color) -> amount * countBags(color) }.sum()
}

fun part2(file: File) = file.readLines().map(::parseRule).toMap()
    .countBags("shiny gold") - 1 // the shiny gold bag itself
