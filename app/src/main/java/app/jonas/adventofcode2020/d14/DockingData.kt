package app.jonas.adventofcode2020.d14

import java.io.File

data class Mask(val value: String) {
    fun mask(input: String) = String(value.mapIndexed { i, m ->
        if (m != 'X') m
        else input.getOrNull(input.length + i - value.length) ?: '0'
    }.toCharArray())

    fun maskAddress(input: String): Set<String> {
        val maskedInput = String(value.mapIndexed { i, m ->
            if (m == '0') input.getOrNull(input.length + i - value.length) ?: '0'
            else m
        }.toCharArray())
        return getAddresses(maskedInput).toSet()
    }

    private fun getAddresses(maskedInput: String): List<String> {
        if (maskedInput == "") return listOf("")
        val firstChar = maskedInput[0]
        val partialAddresses = getAddresses(maskedInput.substring(1))
        return if (firstChar == 'X') {
            partialAddresses.flatMap { address -> listOf("0$address", "1$address") }
        } else {
            partialAddresses.map { address -> firstChar + address}
        }
    }
}

data class Mem(val address: Long, val value: Long)

fun File.parseInput() = readLines().map {
    val (instruction, value) = it.split(" = ")
    if (instruction == "mask") return@map Mask(value)
    val address = instruction.split('[', ']')[1].toLong()
    Mem(address, value.toLong())
}

fun part1(file: File): Long {
    val instructions = file.parseInput()
    var currentMask: Mask? = null
    val map = mutableMapOf<Long, Long>()
    instructions.forEach {
        when (it) {
            is Mask -> currentMask = it
            is Mem -> {
                map[it.address] = currentMask!!.mask(it.value.toString(2)).toLong(2)
            }
        }
    }
    return map.values.sum()
}

fun part2(file: File): Long {
    val instructions = file.parseInput()
    var currentMask: Mask? = null
    val map = mutableMapOf<Long, Long>()
    instructions.forEach {
        when (it) {
            is Mask -> currentMask = it
            is Mem -> {
                val addresses = currentMask!!.maskAddress(it.address.toString(2))
                for (address in addresses) {
                    map[address.toLong(2)] = it.value
                }
            }
        }
    }
    return map.values.sum()
}
