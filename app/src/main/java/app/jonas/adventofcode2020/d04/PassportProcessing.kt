package app.jonas.adventofcode2020.d04

import java.io.File

fun part1(file: File) = file.readText().split(Regex("\n\n")).count {
    it.contains("byr:") &&
            it.contains("iyr:") &&
            it.contains("eyr:") &&
            it.contains("hgt:") &&
            it.contains("hcl:") &&
            it.contains("ecl:") &&
            it.contains("pid:")
}

/*
    byr (Birth Year)        - four digits; at least 1920 and at most 2002.
    iyr (Issue Year)        - four digits; at least 2010 and at most 2020.
    eyr (Expiration Year)   - four digits; at least 2020 and at most 2030.
    hgt (Height)            - a number followed by either cm or in:
        If cm, the number must be at least 150 and at most 193.
        If in, the number must be at least 59 and at most 76.
    hcl (Hair Color)        - a # followed by exactly six characters 0-9 or a-f.
    ecl (Eye Color)         - exactly one of: amb blu brn gry grn hzl oth.
    pid (Passport ID)       - a nine-digit number, including leading zeroes.
    cid (Country ID)        - ignored, missing or not.
*/

fun checkHgt(hgt: String?): Boolean {
    hgt ?: return false
    if (hgt.length < 3) return false
    val value = hgt.substring(0, hgt.length - 2).toInt()
    val unit = hgt.substring(hgt.length - 2)
    return if (unit == "cm") value in 150..193 else if (unit == "in") value in 59..76 else false
}

val hclRegex = Regex("#[0-9a-f]{6}")
fun checkHcl(hcl: String?): Boolean {
    hcl ?: return false
    return hclRegex.matches(hcl)
}

val validEcls = listOf("amb", "blu", "brn", "gry", "grn", "hzl", "oth")
fun checkEcl(ecl: String?): Boolean {
    ecl ?: return false
    return validEcls.count { ecl.contains(it) } == 1
}

// leading 0 is not a requirement
val pidRegex = Regex("[0-9]{9}")
fun checkPid(pid: String?): Boolean {
    pid ?: return false
    return pidRegex.matches(pid)
}

fun part2(file: File) = file.readText().split(Regex("\n\n")).map { passport ->
    passport.split(Regex("[\n ]")).map { field ->
        val res = field.split(Regex(":"))
        res[0] to res[1]
    }.toMap()
}.count {
    it["byr"]?.toInt() in 1920..2002 &&
            it["iyr"]?.toInt() in 2010..2020 &&
            it["eyr"]?.toInt() in 2020..2030 &&
            checkHgt(it["hgt"]) &&
            checkHcl(it["hcl"]) &&
            checkEcl(it["ecl"]) &&
            checkPid(it["pid"])
}