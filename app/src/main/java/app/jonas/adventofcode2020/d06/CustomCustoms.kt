package app.jonas.adventofcode2020.d06

import java.io.File

fun part1(file: File) = file.readText().split(Regex("\n\n")).map { group ->
    val distinct = group.replace("\n", "").toSet()
    distinct.count()
}.sum()

fun part2(file: File) = file.readText().split(Regex("\n\n")).map { group ->
    val people = group.split(Regex("\n"))
    val result = people.map(String::toSet).reduce { acc, answer -> acc.intersect(answer)}
    result.count()
}.sum()
