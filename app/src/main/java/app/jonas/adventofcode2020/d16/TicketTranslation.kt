package app.jonas.adventofcode2020.d16

import java.io.File

fun part1(file: File): Int {
    val (fieldNotes, myTicketNotes, otherTicketsNotes) = file.readText().split("\n\n")
    val fields = extractFields(fieldNotes)
    val otherTickets = extractTickets(otherTicketsNotes)
    var errorRate = 0
    for (ticket in otherTickets) {
        for (value in ticket) {
            val isAllowed = fields.any { field ->
                field.allowedValues.any { allowedValues ->
                    value in allowedValues
                }
            }
            if (!isAllowed) errorRate += value
        }
    }
    return errorRate
}

fun extractTickets(ticketsNotes: String): List<List<Int>> {
    return ticketsNotes.split('\n').drop(1).map { ticketsNote ->
        ticketsNote.split(',').map(String::toInt)
    }
}

data class Field(val name: String, val allowedValues: List<IntRange>)

fun extractFields(fieldNotes: String): List<Field> {
    return fieldNotes.split('\n').map { fieldnote ->
        val splitted = fieldnote.split(": ", "-", " or ")
        val allowedValues = mutableListOf<IntRange>()
        for (i in 1 until splitted.size step 2) {
            allowedValues += splitted[i].toInt()..splitted[i + 1].toInt()
        }
        Field(splitted[0], allowedValues)
    }
}

fun part2(file: File): Long {
    val (fieldNotes, myTicketNotes, otherTicketsNotes) = file.readText().split("\n\n")
    val fields = extractFields(fieldNotes)
    val otherTickets = extractTickets(otherTicketsNotes)
    val myTickets = extractTickets(myTicketNotes)
    val validTickets = myTickets + otherTickets.filter(filterValidTickets(fields))
    val possibilitiesByColumn = getPossibilitiesByColumn(validTickets, fields)
    val departureIndices = extractDepartureIndices(possibilitiesByColumn)
    return departureIndices.fold(1L) { acc, index -> acc * myTickets[0][index] }
}

fun filterValidTickets(fields: List<Field>) = fun(ticket: List<Int>): Boolean {
    for (value in ticket) {
        val isAllowed = fields.any { field ->
            field.allowedValues.any { allowedValues ->
                value in allowedValues
            }
        }
        if (!isAllowed) return false
    }
    return true
}

fun extractDepartureIndices(possibilitiesByColumn: HashMap<Int, MutableList<String>>):
        MutableList<Int> {
    val result = mutableListOf<Int>()
    while (possibilitiesByColumn.count() > 0) {
        for (l in possibilitiesByColumn) {
            if (l.value.size == 1) {
                val r = l.value[0]
                if (r.startsWith("departure ")) {
                    result += l.key
                }
                for (li in possibilitiesByColumn) {
                    li.value.remove(r)
                }
                possibilitiesByColumn.remove(l.key)
                break
            }
        }
    }
    return result
}

private fun getPossibilitiesByColumn(validTickets: List<List<Int>>, fields: List<Field>):
        HashMap<Int, MutableList<String>> {
    val values = transpose(validTickets)
    val map = HashMap<Int, MutableList<String>>(fields.size)
    fields.forEach { field ->
        values.forEachIndexed { i, values ->
            val allAllowed = values.all { value ->
                field.allowedValues.any { allowedValues ->
                    value in allowedValues
                }
            }
            if (allAllowed) {
                map[i] = map[i] ?: mutableListOf()
                map[i]!! += field.name
            }
        }
    }
    return map
}

fun transpose(validTickets: List<List<Int>>): List<List<Int>> {
    val row = validTickets[0].size
    val column = validTickets.size
    val transpose = MutableList(row) { MutableList(column) {-1} }
    for (i in 0 until row) {
        for (j in 0 until column) {
            transpose[i][j] = validTickets[j][i]
        }
    }
    return transpose
}
