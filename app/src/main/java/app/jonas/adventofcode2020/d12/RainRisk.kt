package app.jonas.adventofcode2020.d12

import java.io.File
import kotlin.math.*

data class Vector(var x: Int, var y: Int) {
    fun turnByDegreesLeft(degrees: Int) {
        val rad = degrees * PI / 180
        val newX = round(cos(rad) * x - sin(rad) * y).toInt()
        y = round(sin(rad) * x + cos(rad) * y).toInt()
        x = newX
    }
}

fun part1(file: File): Int {
    val input = parseInput(file)
    var position = Vector(0, 0)
    val direction = Vector(1, 0)
    for (step in input) {
        when (step.first) {
            "N" -> position.y += step.second
            "S" -> position.y -= step.second
            "E" -> position.x += step.second
            "W" -> position.x -= step.second
            "L" -> direction.turnByDegreesLeft(step.second)
            "R" -> direction.turnByDegreesLeft(360 - step.second)
            "F" -> position = Vector(
                position.x + direction.x * step.second,
                position.y + direction.y * step.second
            )
        }
    }
    return abs(position.x) + abs(position.y)
}

fun parseInput(file: File) = file.readLines().map {
    val (operation, value) = Regex("(.)(\\d+)").matchEntire(it)!!.destructured
    operation to value.toInt()
}

fun part2(file: File): Int {
    val input = parseInput(file)
    var position = Vector(0, 0)
    val waypoint = Vector(10, 1)
    for (step in input) {
        when (step.first) {
            "N" -> waypoint.y += step.second
            "S" -> waypoint.y -= step.second
            "E" -> waypoint.x += step.second
            "W" -> waypoint.x -= step.second
            "L" -> waypoint.turnByDegreesLeft(step.second)
            "R" -> waypoint.turnByDegreesLeft(360 - step.second)
            "F" -> position = Vector(
                position.x + waypoint.x * step.second,
                position.y + waypoint.y * step.second
            )
        }
    }
    return abs(position.x) + abs(position.y)
}
