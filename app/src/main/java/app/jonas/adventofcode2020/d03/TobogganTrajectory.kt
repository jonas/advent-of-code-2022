package app.jonas.adventofcode2020.d03

import java.io.File

fun part1(file: File) = file.readLines().fold(0 to 0, { (treeCount, column), line ->
    (if (line[column % line.length] == '#') treeCount + 1 else treeCount) to column + 3
}).let { (treeCount) -> treeCount }

fun part2(file: File): Long {
    val lines = file.readLines()
    return listOf(1 to 1, 3 to 1, 5 to 1, 7 to 1, 1 to 2).map { (right, down) ->
        var treeCount = 0
        var column = 0
        for (row in lines.indices step down) {
            val line = lines[row]
            if (line[column % line.length] == '#') treeCount++
            column += right
        }
        treeCount
    }.fold(1L) { acc, res -> acc * res }
}