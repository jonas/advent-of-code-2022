package app.jonas.adventofcode2020.d15

import java.io.File

fun part1(file: File, numberOfInterest: Int): Int {
    val numbers = file.readText().split(',').map(String::toInt)
    val indices = numbers.associate { it to numbers.lastIndexOf(it) }.toMutableMap()
    var last = numbers.last()
    for (i in numbers.size until numberOfInterest) {
        val before = indices[last]
        indices[last] = i - 1
        last = if (before == null) 0 else i - before - 1
    }
    return last
}
