package app.jonas.adventofcode2020.d02

import java.io.File

//6-13 k: tmsskkjkkvtksnm
val pattern = Regex("(\\d+)-(\\d+) (.): (.*)")

fun part1(file: File) = file.readLines().fold(0) { acc, line ->
    //[1-3 a: abcde, 1, 3, a, abcde]
    val result = pattern.matchEntire(line)?.groupValues ?: return acc
    val min = result[1].toInt()
    val max = result[2].toInt()
    val char = result[3][0]
    val password = result[4]
    val count = password.count{ it == char }
    if (count in min..max) acc + 1 else acc
}

fun part2(file: File) = file.readLines().fold(0) { acc, line ->
    //[1-3 a: abcde, 1, 3, a, abcde]
    val result = pattern.matchEntire(line)?.groupValues ?: return acc
    val pos0 = result[1].toInt() - 1
    val pos1 = result[2].toInt() - 1
    val char = result[3][0]
    val password = result[4]
    val count = listOf(password[pos0], password[pos1]).count{ it == char }
    if (count == 1) acc + 1 else acc
}