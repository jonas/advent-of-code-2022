package app.jonas.adventofcode2020.d08

import java.io.File

fun File.parseInstructions() = readLines().map {
    val (operation, argument) = it.split(' ')
    operation to argument.toInt()
}

fun part1(file: File): Int {
    var acc = 0
    var index = 0
    val instructions = file.parseInstructions()
    val visited = mutableSetOf<Int>()

    while (!visited.contains(index)) {
        visited += index
        val instruction = instructions[index]
        when (instruction.first) {
            "nop" -> index++
            "acc" -> {
                acc += instruction.second
                index++
            }
            "jmp" -> index += instruction.second
        }
    }
    return acc
}

fun tryWithChange(instructions: List<Pair<String, Int>>, stepToChange: Int): Pair<Boolean, Int> {
    var step = 0
    var acc = 0
    var index = 0
    val visited = mutableSetOf<Int>()

    while (!visited.contains(index)) {
        if (index == instructions.size) return true to acc
        visited += index
        val instruction = instructions[index]
        val operation = if (step == stepToChange)  {
            when (instruction.first) {
                "nop" -> "jmp"
                "jmp" -> "nop"
                else -> instruction.first
            }
        } else instruction.first
        when (operation) {
            "nop" -> index++
            "acc" -> {
                acc += instruction.second
                index++
            }
            "jmp" -> index += instruction.second
        }
        step++
    }
    return false to acc
}

fun part2(file: File): Int {
    val instructions = file.parseInstructions()
    var stepToChange = 0
    while (true) {
        val (res, acc) = tryWithChange(instructions, stepToChange)
        if (res) return acc
        stepToChange++
    }
}
