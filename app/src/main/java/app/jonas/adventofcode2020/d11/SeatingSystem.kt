package app.jonas.adventofcode2020.d11

import java.io.File

fun part1(file: File): Int {
    val ferry = file.readLines().map(String::toList)
    val finalPlacement = predictStableFerryState(ferry, 4, ::calculateAdjacentCount)
    return finalPlacement.map { row -> row.count { it == '#' } }.sum()
}

fun predictStableFerryState(
    ferry: List<List<Char>>,
    requiredAdjacentCount: Int,
    calculateAdjacentCount: (row: Int, column: Int, ferry: List<List<Char>>) -> Int
): List<List<Char>> {
    val newPlacement = List(ferry.size) { MutableList(ferry[0].size) { '.' } }
    var changed = false
    for (row in ferry.indices) {
        for (column in ferry[row].indices) {
            val seat = ferry[row][column]
            if (seat == '.') {
                newPlacement[row][column] = seat
                continue
            }

            val adjacentCount = calculateAdjacentCount(row, column, ferry)

            newPlacement[row][column] = if (seat == 'L') {
                if (adjacentCount == 0) {
                    changed = true
                    '#'
                } else 'L'
            } else {
                if (adjacentCount >= requiredAdjacentCount) {
                    changed = true
                    'L'
                } else '#'
            }
        }
    }
    return if (changed)
        predictStableFerryState(newPlacement, requiredAdjacentCount, calculateAdjacentCount)
    else newPlacement
}

fun calculateAdjacentCount(
    row: Int,
    column: Int,
    ferry: List<List<Char>>
): Int {
    var adjacentCount = 0
    for (r in (row - 1)..(row + 1)) {
        for (c in (column - 1)..(column + 1)) {
            if (r == row && c == column) continue
            if (ferry.getOrNull(r)?.getOrNull(c) == '#') adjacentCount++
        }
    }
    return adjacentCount
}

fun calculateAdjacentCountSeeing(
    row: Int,
    column: Int,
    ferry: List<List<Char>>
): Int {
    var adjacentCount = 0
    for (r in (row - 1)..(row + 1)) {
        for (c in (column - 1)..(column + 1)) {
            if (r == row && c == column) continue
            var s = ferry.getOrNull(r)?.getOrNull(c)
            val rowDistance = row - r
            val columnDistance = column - c
            var scaleFactor = 1
            while (s == '.') {
                scaleFactor++
                s = ferry.getOrNull(row - rowDistance * scaleFactor)
                    ?.getOrNull(column - columnDistance * scaleFactor)
            }
            if (s == '#') adjacentCount++
        }
    }
    return adjacentCount
}

fun part2(file: File): Int {
    val ferry = file.readLines().map(String::toList)
    val finalPlacement = predictStableFerryState(ferry, 5, ::calculateAdjacentCountSeeing)
    return finalPlacement.map { row -> row.count { it == '#' } }.sum()
}
