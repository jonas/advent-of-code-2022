package app.jonas.adventofcode2020.d18

import java.io.File

fun part1(file: File) = file.readLines().map { calculate(it) }.sum()

// 1 + (2 * 3) + (4 * (5 + 6))
fun calculate(input: String, advanced: Boolean = false): Long {
    // most simple: just a number -> parse and return
    // one operation -> calculate and return
    // parentheses -> find inner most -> recursive call calculate -> do while parentheses exist
    val closingIndex = input.indexOf(')')
    if (closingIndex == -1) {
        val plusIndex = input.indexOf('+')
        if (!advanced || plusIndex == -1) {
            return calculateFromLeftToRight(input)
        }
        return calculatePluses(input, plusIndex)
    }
    return calculateParentheses(input, closingIndex, advanced)
}

fun calculatePluses(input: String, plusIndex: Int): Long {
    val firstNumber = input.substring(0, plusIndex - 1).split(' ').last()
    val secondNumber = input.substring(plusIndex + 2).split(' ').first()
    return calculate(
        "${
            input.substring(0, plusIndex - 1 - firstNumber.length)
        }${
            firstNumber.toLong() + secondNumber.toLong()
        }${
            input.substring(plusIndex + 1 + secondNumber.length + 1)
        }",
        true
    )
}

private fun calculateParentheses(input: String, closingIndex: Int, advanced: Boolean): Long {
    val substring = input.substring(0, closingIndex)
    val openingIndex = substring.lastIndexOf('(')
    return calculate(
        "${
            input.substring(0, openingIndex)
        }${
            calculate(substring.substring(openingIndex + 1), advanced)
        }${
            input.substring(closingIndex + 1)
        }",
        advanced
    )
}

fun calculateFromLeftToRight(input: String): Long {
    var lastOperator = "+"
    return input.split(' ').fold(0L) { acc, numberOrOperator ->
        when (numberOrOperator) {
            "+", "*" -> lastOperator = numberOrOperator
            else -> return@fold when (lastOperator) {
                "+" -> acc + numberOrOperator.toLong()
                "*" -> acc * numberOrOperator.toLong()
                else -> throw IllegalArgumentException()
            }
        }
        acc
    }
}

fun part2(file: File) = file.readLines().map { calculate(it, true) }.sum()
