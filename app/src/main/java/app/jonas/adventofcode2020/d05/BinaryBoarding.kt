package app.jonas.adventofcode2020.d05

import java.io.File

fun boardingPassToSeatID(boardingPass: String): Int {// BFFFBBFRRR
    val row = boardingPass
        .substring(0, 7)
        .replace('B', '1')
        .replace('F', '0')
        .toInt(2)
    val column = boardingPass
        .substring(7)
        .replace('R', '1')
        .replace('L', '0')
        .toInt(2)
    return row * 8 + column
}

fun part1(file: File) = file.readLines().maxOfOrNull(::boardingPassToSeatID)

fun part2(file: File): Int {
    val seatIDs = file.readLines().map(::boardingPassToSeatID)
    val result = seatIDs.first { seatID ->
        seatIDs.firstOrNull { s -> s == seatID + 1 } == null &&
                seatIDs.firstOrNull { s -> s == seatID + 2 } != null
    }
    return result + 1 // we need to find the empty seat
}
