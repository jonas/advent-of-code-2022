package app.jonas.adventofcode2020.d01

import java.io.File

fun part1(file: File): Int {
    val prev = mutableListOf<Int>()
    file.readLines().forEach { s ->
        val current = s.toInt()
        prev.forEach { p -> if (p + current == 2020) return p * current }
        prev += current
    }
    return -1
}

fun part2(file: File): Int {
    val prev = mutableListOf<Int>()
    file.readLines().forEach { s ->
        val current = s.toInt()
        prev.forEach { p1 ->
            // might be the same number again
            prev.forEach { p2 ->
                if (p1 + p2 + current == 2020) return p1 * p2 * current
            }
        }
        prev += current
    }
    return -1
}