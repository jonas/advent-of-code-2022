package app.jonas.adventofcode2020.d17

import java.io.File

typealias Cube = List<List<List<Char>>>

fun Cube.print() {
    for (z in this) {
        for (y in z) {
            println(String(y.toCharArray()))
        }
        println("".repeat(z.size))
    }
}

fun List<List<Char>>.toCube(roundsOfLife: Int): Cube {
    val maxGrowing = 2 * roundsOfLife
    val x = get(0).size + maxGrowing
    val y = size + maxGrowing
    val z = 1 + maxGrowing
    return List(z) { cz ->
        List(y) { cy ->
            List(x) { cx ->
                val value = if (cz == roundsOfLife) {
                    getOrNull(cy - roundsOfLife)?.getOrNull(cx - roundsOfLife)
                } else {
                    null
                }
                value ?: '.'
            }
        }
    }
}

fun part1(file: File): Int {
    val input = file.readLines().map(String::toList)
    val roundsOfLife = 6
    var cube = input.toCube(roundsOfLife)
    for (r in 1..roundsOfLife) {
        cube = cube.evolve()
    }
    // cube.print()
    return cube.countActive()
}

private fun Cube.countActive(): Int {
    return flatten().flatten().count { it == '#' }
}

fun Cube.evolve(): Cube {
    return List(size) { z ->
        List(get(0).size) { y ->
            List(get(0)[0].size) { x ->
                val adjacentCount = calculateAdjacentCount(x, y, z)
                if (get(z)[y][x] == '#') {
                    if (adjacentCount in 2..3) {
                        '#'
                    } else '.'
                } else {
                    if (adjacentCount == 3) {
                        '#'
                    } else '.'
                }
            }
        }
    }
}

fun Cube.calculateAdjacentCount(x: Int, y: Int, z: Int): Int {
    var adjacentCount = 0
    for (cz in (z - 1)..(z + 1)) {
        for (cy in (y - 1)..(y + 1)) {
            for (cx in (x - 1)..(x + 1)) {
                if (x == cx && y == cy && z == cz) continue
                if (getOrNull(cz)?.getOrNull(cy)?.getOrNull(cx) == '#') adjacentCount++
            }
        }
    }
    return adjacentCount
}

typealias HyperCube = List<List<List<List<Char>>>>

fun List<List<Char>>.toHyperCube(roundsOfLife: Int): HyperCube {
    val maxGrowing = 2 * roundsOfLife
    val x = get(0).size + maxGrowing
    val y = size + maxGrowing
    val z = 1 + maxGrowing
    val w = 1 + maxGrowing
    return List(w) { cw ->
        List(z) { cz ->
            List(y) { cy ->
                List(x) { cx ->
                    val value = if (cw == roundsOfLife && cz == roundsOfLife) {
                        getOrNull(cy - roundsOfLife)?.getOrNull(cx - roundsOfLife)
                    } else {
                        null
                    }
                    value ?: '.'
                }
            }
        }
    }
}

fun part2(file: File): Int {
    val input = file.readLines().map(String::toList)
    val roundsOfLife = 6
    var cube = input.toHyperCube(roundsOfLife)
    for (r in 1..roundsOfLife) {
        cube = cube.evolve()
    }
    // cube.print()
    return cube.countActive()
}

@JvmName("hyperCountActive")
private fun HyperCube.countActive(): Int {
    return flatten().flatten().flatten().count { it == '#' }
}

@JvmName("hyperEvolve")
fun HyperCube.evolve(): HyperCube {
    return List(size) { w ->
        List(get(0).size) { z ->
            List(get(0)[0].size) { y ->
                List(get(0)[0][0].size) { x ->
                    val adjacentCount = calculateAdjacentCount(x, y, z, w)
                    if (get(w)[z][y][x] == '#') {
                        if (adjacentCount in 2..3) {
                            '#'
                        } else '.'
                    } else {
                        if (adjacentCount == 3) {
                            '#'
                        } else '.'
                    }
                }
            }
        }
    }
}

@JvmName("hyperCalculateAdjacentCount")
fun HyperCube.calculateAdjacentCount(x: Int, y: Int, z: Int, w: Int): Int {
    var adjacentCount = 0
    for (cw in (w - 1)..(w + 1)) {
        for (cz in (z - 1)..(z + 1)) {
            for (cy in (y - 1)..(y + 1)) {
                for (cx in (x - 1)..(x + 1)) {
                    if (x == cx && y == cy && z == cz && w == cw) continue
                    if (getOrNull(cw)?.getOrNull(cz)?.getOrNull(cy)?.getOrNull(cx) == '#')
                        adjacentCount++
                }
            }
        }
    }
    return adjacentCount
}
