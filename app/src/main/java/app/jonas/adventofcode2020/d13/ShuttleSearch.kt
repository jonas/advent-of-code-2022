package app.jonas.adventofcode2020.d13

import java.io.File
import java.math.BigInteger
import java.math.BigInteger.ZERO

fun File.parseInput(): Pair<Int, List<Int>> {
    val (earliest, buses) = readLines()
    return earliest.toInt() to buses.split(',').filter { it != "x" }.map(String::toInt)
}

fun part1(file: File): Int {
    val (earliest, buses) = file.parseInput()
    val busesByWaitingTime = buses.associateBy { it - earliest % it }.toSortedMap()
    val timeToWait = busesByWaitingTime.firstKey()
    val bus = busesByWaitingTime[timeToWait] ?: throw IllegalArgumentException()
    return bus * timeToWait
}

fun File.parseInput2(): List<Pair<BigInteger, BigInteger>> {
    val (_, buses) = readLines()
    return buses.split(',').foldIndexed(mutableListOf()) { offset, acc, bus ->
        if (bus != "x") {
            acc += bus.toBigInteger() to offset.toBigInteger()
        }
        acc
    }
}

fun part2(file: File): BigInteger {
    val buses = file.parseInput2()
    // a variable step size was the key thing I was missing before
    // https://todd.ginsberg.com/post/advent-of-code/2020/day13/
    var stepSize = buses.first().first
    var i = ZERO
    buses.drop(1).forEach {(bus, offset) ->
        while ((i + offset) % bus != ZERO) i += stepSize
        stepSize *= bus
    }
    return i
}
