package app.jonas.adventofcode2020.d10

import java.io.File

fun part1(file: File): Int {
    val numbers = file.readLines().map(String::toInt)
    val sorted = numbers.sorted().toMutableList()
    sorted.add(0, 0)
    sorted += sorted[sorted.size - 1] + 3
    val differences = sorted.zipWithNext { a, b -> b - a }.groupBy { it }
    return differences[1]!!.count() * differences[3]!!.count()
}

fun part2(file: File): Long {
    val numbers = file.readLines().map(String::toInt)
    val sorted = numbers.sorted().toMutableList()
    sorted.add(0, 0)
    sorted += sorted[sorted.size - 1] + 3
    return checkPossibilities(sorted.toList())
}

fun checkPossibilities(sorted: List<Int>): Long {
    for (i in 0 until (sorted.size - 1)) { // our notebook must not be checked
        val current = sorted[i]
        val next = sorted[i + 1] // never null (last = notebook)
        if (next - current >= 3) continue

        val after = sorted.getOrNull(i + 2) ?: continue // if after null: next == notebook
        if (after - current > 3) continue

        val endIndex = findNextFixPointIndex(sorted, i + 1)

        val without = listOf(current) + sorted.subList(i + 2, endIndex + 1)
        return (checkPossibilities(without)
                + checkPossibilities(sorted.subList(i + 1, endIndex + 1))) *
                checkPossibilities(sorted.subList(endIndex, sorted.size))
    }
    return 1
}

fun findNextFixPointIndex(sorted: List<Int>, startIndex: Int): Int {
    for (j in startIndex until sorted.size) {
        if (j == sorted.size - 1) break
        val currentInSubList = sorted[j]
        val nextInSubList = sorted[j + 1]
        if (nextInSubList - currentInSubList >= 3) {
            return j
        }
    }
    return sorted.size - 1
}
