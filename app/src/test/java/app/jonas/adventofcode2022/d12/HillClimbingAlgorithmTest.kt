package app.jonas.adventofcode2022.d12

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class HillClimbingAlgorithmTest {
    @Test
    fun testPart1Example() = assertEquals(31L, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(0L, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(0L, part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals(0L, part2(load("input.txt")))
}
