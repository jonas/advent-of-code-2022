package app.jonas.adventofcode2022.d03

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class RucksackReorganizationTest {
    @Test
    fun testPart1Example() = assertEquals(157, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(7428, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(70, part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals(2650, part2(load("input.txt")))
}
