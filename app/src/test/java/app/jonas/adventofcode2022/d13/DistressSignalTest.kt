package app.jonas.adventofcode2022.d13

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class DistressSignalTest {
    @Test
    fun testPart1Example() = assertEquals(13L, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(5003L, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(140L, part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals(0L, part2(load("input.txt")))
}
