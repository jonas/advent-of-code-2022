package app.jonas.adventofcode2022.d17

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class PyroclasticFlowTest {
    @Test
    fun testPart1Example() = assertEquals(3068L, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(3159L, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(1_514_285_714_288L, part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals(0L, part2(load("input.txt")))
}
