package app.jonas.adventofcode2022.d09

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class RopeBridgeTest {
    @Test
    fun testPart1Example() = assertEquals(13L, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(6357L, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(1L, part2(load("example.txt")))

    @Test
    fun testPart2Example2() = assertEquals(36L, part2(load("example2.txt")))

    @Test
    fun testPart2Input() = assertEquals(2627L, part2(load("input.txt")))
}
