package app.jonas.adventofcode2022.d10

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class CathodeRayTubeTest {
    @Test
    fun testPart1Example() = assertEquals(13140L, part1(load("example.txt")))

    @Test
    fun testPart1Example2() = assertEquals(0L, part1(load("example2.txt")))

    @Test
    fun testPart1Input() = assertEquals(14620L, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals("""
        ##..##..##..##..##..##..##..##..##..##..
        ###...###...###...###...###...###...###.
        ####....####....####....####....####....
        #####.....#####.....#####.....#####.....
        ######......######......######......####
        #######.......#######.......#######.....
    """.trimIndent(), part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals("""
        ###....##.####.###..#..#.###..####.#..#.
        #..#....#.#....#..#.#..#.#..#.#....#..#.
        ###.....#.###..#..#.####.#..#.###..#..#.
        #..#....#.#....###..#..#.###..#....#..#.
        #..#.#..#.#....#.#..#..#.#.#..#....#..#.
        ###...##..#....#..#.#..#.#..#.#.....##..
    """.trimIndent(), part2(load("input.txt")))
}
