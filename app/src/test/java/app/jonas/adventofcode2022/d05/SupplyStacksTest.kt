package app.jonas.adventofcode2022.d05

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class SupplyStacksTest {
    @Test
    fun testPart1Example() = assertEquals("CMZ", part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals("DHBJQJCCW", part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals("MCD", part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals("WJVRLSJJT", part2(load("input.txt")))
}
