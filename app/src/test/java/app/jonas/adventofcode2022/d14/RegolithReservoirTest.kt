package app.jonas.adventofcode2022.d14

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class RegolithReservoirTest {
    @Test
    fun testPart1Example() = assertEquals(24L, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(779L, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(93L, part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals(0L, part2(load("input.txt")))
}
