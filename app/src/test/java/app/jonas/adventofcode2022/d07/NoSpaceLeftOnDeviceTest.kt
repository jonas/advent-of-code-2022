package app.jonas.adventofcode2022.d07

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class NoSpaceLeftOnDeviceTest {
    @Test
    fun testPart1Example() = assertEquals(95437L, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(1743217L, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(24933642L, part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals(8319096L, part2(load("input.txt")))
}
