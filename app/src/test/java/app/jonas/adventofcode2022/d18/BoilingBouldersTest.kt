package app.jonas.adventofcode2022.d18

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class BoilingBouldersTest {
    @Test
    fun testPart1ExampleTiny() = assertEquals(10L, part1(load("example-tiny.txt")))

    @Test
    fun testPart1Example() = assertEquals(64L, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(4512L, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(58L, part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals(2554L, part2(load("input.txt")))
}
