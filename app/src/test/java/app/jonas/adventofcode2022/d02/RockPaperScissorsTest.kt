package app.jonas.adventofcode2022.d02

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class RockPaperScissorsTest {
    @Test
    fun testPart1Example() = assertEquals(15, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(13268, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(12, part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals(15508, part2(load("input.txt")))
}
