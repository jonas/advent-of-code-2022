package app.jonas.adventofcode2022.d06

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class TuningTroubleTest {
    @Test
    fun testPart1Example() = assertEquals(7L, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(1929L, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(19L, part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals(3298L, part2(load("input.txt")))
}
