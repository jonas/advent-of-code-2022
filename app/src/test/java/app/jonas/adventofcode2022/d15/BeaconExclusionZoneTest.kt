package app.jonas.adventofcode2022.d15

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class BeaconExclusionZoneTest {
    @Test
    fun testPart1Example() = assertEquals(26L, part1(load("example.txt"), 10L))

    @Test
    fun testPart1Input() = assertEquals(5367037L, part1(load("input.txt"), 2_000_000L))

    @Test
    fun testPart2Example() = assertEquals(56000011L, part2(load("example.txt"), 20L))

    @Test
    fun testPart2Input() = assertEquals(0L, part2(load("input.txt"), 4_000_000L))
}
