package app.jonas.adventofcode2022.d08

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class TreetopTreeHouseTest {
    @Test
    fun testPart1Example() = assertEquals(21L, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(1851L, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(8L, part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals(574080L, part2(load("input.txt")))
}
