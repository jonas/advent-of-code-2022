package app.jonas.adventofcode2022.d11

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test
import java.math.BigInteger

class MonkeyInTheMiddleTest {
    @Test
    fun testPart1Example() = assertEquals(BigInteger("10605"), part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(BigInteger("107822"), part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(BigInteger("2713310158"), part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals(BigInteger("27267163742"), part2(load("input.txt")))
}
