package app.jonas.adventofcode2020.d12

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class RainRiskTest {
    @Test
    fun testPart1Example() = assertEquals(25, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(1838, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(286, part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals(2128, part2(load("input.txt")))
}
