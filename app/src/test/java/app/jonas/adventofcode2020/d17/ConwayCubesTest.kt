package app.jonas.adventofcode2020.d17

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class ConwayCubesTest {
    @Test
    fun testPart1Example() = assertEquals(112, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(252, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(848, part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals(2160, part2(load("input.txt")))
}
