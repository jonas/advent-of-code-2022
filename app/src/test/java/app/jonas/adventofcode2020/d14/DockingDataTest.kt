package app.jonas.adventofcode2020.d14

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class DockingDataTest {
    @Test
    fun testPart1Example() = assertEquals(165, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(12408060320841, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(404, part2(load("simple.txt")))

    @Test
    fun testPart2Input() = assertEquals(4466434626828, part2(load("input.txt")))
}
