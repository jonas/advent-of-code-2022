package app.jonas.adventofcode2020.d10

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class AdapterArrayTest {
    @Test
    fun testPart1Simple() = assertEquals(35, part1(load("simple.txt")))

    @Test
    fun testPart1Example() = assertEquals(220, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(2400, part1(load("input.txt")))

    @Test
    fun testPart2Simple() = assertEquals(8, part2(load("simple.txt")))

    @Test
    fun testPart2Example() = assertEquals(19208, part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals(338510590509056, part2(load("input.txt")))
}
