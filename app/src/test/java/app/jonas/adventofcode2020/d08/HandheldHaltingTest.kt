package app.jonas.adventofcode2020.d08

import app.jonas.common.load
import org.junit.Assert.assertFalse
import org.junit.Assert.assertEquals
import org.junit.Test

class HandheldHaltingTest {
    @Test
    fun testPart1Example() = assertEquals(5, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(1684, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(8, part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals(2188, part2(load("input.txt")))
}
