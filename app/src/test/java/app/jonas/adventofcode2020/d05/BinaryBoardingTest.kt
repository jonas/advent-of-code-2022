package app.jonas.adventofcode2020.d05

import app.jonas.common.load
import org.junit.Assert.assertFalse
import org.junit.Assert.assertEquals
import org.junit.Test

class BinaryBoardingTest {
    @Test
    fun testPart1Example() = assertEquals(820, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(965, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(3, part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals(524, part2(load("input.txt")))
}
