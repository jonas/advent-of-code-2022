package app.jonas.adventofcode2020.d09

import app.jonas.common.load
import org.junit.Assert.assertFalse
import org.junit.Assert.assertEquals
import org.junit.Test

class EncodingErrorTest {
    @Test
    fun testPart1Example() = assertEquals(127, part1(load("example.txt"), 5))

    @Test
    fun testPart1Input() = assertEquals(1124361034, part1(load("input.txt"), 25))

    @Test
    fun testPart2Example() = assertEquals(62, part2(load("example.txt"), 5))

    @Test
    fun testPart2Input() = assertEquals(129444555, part2(load("input.txt"), 25))
}
