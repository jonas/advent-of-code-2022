package app.jonas.adventofcode2020.d01

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class ReportRepairTest {
    @Test
    fun testPart1Example() = assertEquals(514579, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(326211, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(241861950, part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals(131347190, part2(load("input.txt")))
}
