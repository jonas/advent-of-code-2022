package app.jonas.adventofcode2020.d03

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class TobogganTrajectoryTest {
    @Test
    fun testPart1Example() = assertEquals(7, part1(load("example.txt")))

    @Test
    fun testPart1Simple() = assertEquals(1, part1(load("simple.txt")))

    @Test
    fun testPart1Input() = assertEquals(156, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(336, part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals(3521829480, part2(load("input.txt")))
}
