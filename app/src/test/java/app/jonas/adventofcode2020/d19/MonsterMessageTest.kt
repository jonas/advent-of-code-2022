package app.jonas.adventofcode2020.d19

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class MonsterMessageTest {
    @Test
    fun testPart1Example() = assertEquals(
        71 + 51 + 26 + 437 + 12240 + 13632,
        part1(load("example.txt"))
    )

    @Test
    fun testPart1Input() = assertEquals(5019432542701, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(
        231 + 51 + 46 + 1445 + 669060 + 23340,
        part2(load("example.txt"))
    )

    @Test
    fun testPart2Input() = assertEquals(70518821989947, part2(load("input.txt")))
}
