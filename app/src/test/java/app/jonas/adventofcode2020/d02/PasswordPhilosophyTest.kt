package app.jonas.adventofcode2020.d02

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class PasswordPhilosophyTest {
    @Test
    fun testPart1Example() = assertEquals(2, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(538, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(1, part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals(489, part2(load("input.txt")))
}
