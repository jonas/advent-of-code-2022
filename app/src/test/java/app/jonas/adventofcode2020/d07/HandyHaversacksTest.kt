package app.jonas.adventofcode2020.d07

import app.jonas.common.load
import org.junit.Assert.assertFalse
import org.junit.Assert.assertEquals
import org.junit.Test

class HandyHaversacksTest {
    @Test
    fun testPart1Example() = assertEquals(4, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(115, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(126, part2(load("example2.txt")))

    @Test
    fun testPart2Input() = assertEquals(1250, part2(load("input.txt")))
}
