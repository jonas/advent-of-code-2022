package app.jonas.adventofcode2020.d11

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class SeatingSystemTest {
    @Test
    fun testPart1Example() = assertEquals(37, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(2316, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(26, part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals(2128, part2(load("input.txt")))
}
