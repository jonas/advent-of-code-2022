package app.jonas.adventofcode2020.d15

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class RambunctiousRecitationTest {
    @Test
    fun testPart1Example() = assertEquals(436, part1(load("example.txt"), 2020))

    @Test
    fun testPart1Input() = assertEquals(410, part1(load("input.txt"), 2020))

    @Test
    fun testPart2Example() = assertEquals(175594, part1(load("example.txt"), 30000000))

    @Test
    fun testPart2Input() = assertEquals(238, part1(load("input.txt"), 30000000))
}
