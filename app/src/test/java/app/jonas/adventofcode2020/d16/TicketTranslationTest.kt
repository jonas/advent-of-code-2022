package app.jonas.adventofcode2020.d16

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class TicketTranslationTest {
    @Test
    fun testPart1Example() = assertEquals(71, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(21978, part1(load("input.txt")))

    @Test
    fun testPart2Input() = assertEquals(1053686852011, part2(load("input.txt")))
}
