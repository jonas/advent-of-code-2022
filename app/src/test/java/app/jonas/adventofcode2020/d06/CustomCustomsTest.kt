package app.jonas.adventofcode2020.d06

import app.jonas.common.load
import org.junit.Assert.assertFalse
import org.junit.Assert.assertEquals
import org.junit.Test

class CustomCustomsTest {
    @Test
    fun testPart1Example() = assertEquals(11, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(6530, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(6, part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals(3323, part2(load("input.txt")))
}
