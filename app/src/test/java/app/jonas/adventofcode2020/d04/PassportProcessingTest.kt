package app.jonas.adventofcode2020.d04

import app.jonas.common.load
import org.junit.Assert.assertFalse
import org.junit.Assert.assertEquals
import org.junit.Test

class PassportProcessingTest {
    @Test
    fun testPart1Example() = assertEquals(2, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(219, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(3, part2(load("part2.txt")))

    @Test
    fun testHgt1() = assert(checkHgt("60in"))

    @Test
    fun testHgt2() = assert(checkHgt("190cm"))

    @Test
    fun testHgt3() = assertFalse(checkHgt("190in"))

    @Test
    fun testHgt4() = assertFalse(checkHgt("190"))

    @Test
    fun testHcl1() = assert(checkHcl("#123abc"))

    @Test
    fun testHcl2() = assertFalse(checkHcl("#123abz"))

    @Test
    fun testHcl3() = assertFalse(checkHcl("123abc"))

    @Test
    fun testEcl1() = assert(checkEcl("brn"))

    @Test
    fun testEcl2() = assertFalse(checkEcl("wat"))

    @Test
    fun testPid1() = assert(checkPid("000000001"))

    @Test
    fun testPid2() = assertFalse(checkPid("0123456789"))

    @Test
    fun testPid3() = assert(checkPid("500000001"))

    @Test
    fun testPart2Input() = assertEquals(127, part2(load("input.txt")))
}
