package app.jonas.adventofcode2020.d13

import app.jonas.common.load
import org.junit.Assert.assertEquals
import org.junit.Test

class ShuttleSearchTest {
    @Test
    fun testPart1Example() = assertEquals(295, part1(load("example.txt")))

    @Test
    fun testPart1Input() = assertEquals(4782, part1(load("input.txt")))

    @Test
    fun testPart2Example() = assertEquals(1068781.toBigInteger(), part2(load("example.txt")))

    @Test
    fun testPart2Input() = assertEquals(1118684865113056.toBigInteger(), part2(load("input.txt")))
}
